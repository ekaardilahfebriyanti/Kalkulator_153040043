/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ujian;

import java.util.Scanner;

/**
 *
 * @author SB601-38
 */
public class Kalkulator {
    
    public int tambah(int a, int b){
        return a+b;
    }
    
    public int kurang(int a, int b){
        return a-b;
    }
    
    public int kali(int a, int b){
        return a*b;
    }
    
    public int bagi(int a, int b){
        return a/b;
    }
    
    
    public static void main(String[] args) {
        Kalkulator k = new Kalkulator();
        
        System.out.println("Hasil tambah : "+k.tambah(2, 3));
        System.out.println("Hasil kurang : "+k.kurang(3, 1));
        System.out.println("Hasil kali : "+k.kali(2, 3));
        System.out.println("Hasil bagi : "+k.bagi(4, 2));
    }
}
