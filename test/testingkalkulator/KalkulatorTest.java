package testingkalkulator;



import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import ujian.Kalkulator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author SB601-38
 */
public class KalkulatorTest {
    Kalkulator k = new Kalkulator();
    @Test
    public void testTambah(){
        assertEquals(5, k.tambah(3, 2),0);
    }
    
    @Test
    public void testkurang(){
        assertEquals(2, k.kurang(3, 1),0);
    }
    
    @Test
    public void testKali(){
        assertEquals(10, k.kali(2, 5),0);
    }
    
    @Test
    public void testBagi(){
        assertEquals(3, k.bagi(6, 2),0);
    }
    
    public static void main(String[] args) {
        KalkulatorTest kt = new KalkulatorTest();
        kt.testTambah();
        kt.testkurang();
        kt.testKali();
        kt.testBagi();
    }
    
}
